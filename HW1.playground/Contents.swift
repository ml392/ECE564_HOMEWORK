import UIKit

/*:
 ### ECE 564 HW1 assignment
 In this first assignment, you are going to create a base data model for storing information about the students, TAs and professors in ECE 564. You need to populate your data model with at least 3 records, but can add more.  You will also provide a search function ("whoIs") to search an array of those objects by first name and last name.
 I suggest you create a new class called `DukePerson` and have it subclass `Person`.  You also need to abide by 2 protocols:
 1. BlueDevil
 2. CustomStringConvertible
 
 I also suggest you try to follow good OO practices by containing any properties and methods that deal with `DukePerson` within the class definition.
 */
/*:
 In addition to the properties required by `Person`, `CustomStringConvertible` and `BlueDevil`, you need to include the following information about each person:
 * Their degree, if applicable
 * Up to 3 of their best programming languages as an array of `String`s (like `hobbies` that is in `BlueDevil`)
 */
/*:
 I suggest you create an array of `DukePerson` objects, and you **must** have at least 3 entries in your array for me to test:
 1. Yourself
 2. Me (my info is in the slide deck)
 3. One of the TAs (also in slide deck)
 */
/*:
 Your program must contain the following:
 - You must include 4 of the following - array, dictionary, set, class, function, closure expression, enum, struct
 - You must include 4 different types, such as string, character, int, double, bool, float
 - You must include 4 different control flows, such as for/in, while, repeat/while, if/else, switch/case
 - You must include 4 different operators from any of the various categories of assignment, arithmatic, comparison, nil coalescing, range, logical
 */
/*: 
 Base grade is a 45 but more points can be earned by adding additional functions besides whoIs (like additional search), extensive error checking, concise code, excellent OO practices, and/or excellent, unique algorithms
 */
/*:
 Below is an example of what the string output from `whoIs' should look like:
 
     Ric Telford is from Morrisville, NC and is a Professor. He is proficient in Swift, C and C++. When not in class, Ric enjoys Biking, Hiking and Golf.
 */

enum Gender {
    case Male
    case Female
}

class Person {
    var firstName = "First"
    var lastName = "Last"
    var whereFrom = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
}

enum DukeRole : String {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}

protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}
//: ### START OF HOMEWORK 
//: Do not change anything above.
//: Put your code here:

class DukePerson : Person, BlueDevil, CustomStringConvertible {
    var hobbies = [String]()
    var role : DukeRole = .Student
    var degree = "None"
    var programmingLanguage = [String]()
    
    init(firstName: String, lastName : String, whereFrom : String, gender : Gender, role : DukeRole, degree : String, hobbies : [String], programmingLanguage : [String]) {
        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
        self.role = role
        self.degree = degree
        self.hobbies = hobbies
        self.programmingLanguage = programmingLanguage
    }
    
    var description: String {
        return "DukePerson stores the information of persons of Duke University. The information includes first name, last name, hometown, gender, role at Duke, degree, hobbies (no more than 3) and preferred programming languages (no more than 3)."
    }
}

let testPerson1 = DukePerson(firstName: "Ric",
                             lastName : "Telford",
                             whereFrom : "Morrisville, NC",
                             gender : .Male,
                             role : .Professor,
                             degree : "BS",
                             hobbies : ["Biking" , "Hiking", "Golf"],
                             programmingLanguage : ["Swift", "C", "C++"])

let testPerson2 = DukePerson(firstName: "Menglin",
                             lastName : "Liu",
                             whereFrom : "Zhengzhou, China",
                             gender : .Female,
                             role : .Student,
                             degree : "Master",
                             hobbies : ["Singing" , "Painting", "Basketball"],
                             programmingLanguage : ["C++", "Java"])

let testPerson3 = DukePerson(firstName: "Niral",
                             lastName : "Shah",
                             whereFrom : "Central New Jersey",
                             gender : .Male,
                             role : .TA,
                             degree : "Master",
                             hobbies : ["Computer Vision projects", "Tennis", "Traveling"],
                             programmingLanguage : ["Swift", "Python", "Java"])

let testPerson4 = DukePerson(firstName: "No",
                             lastName : "Language",
                             whereFrom : "Central New Jersey",
                             gender : .Male,
                             role : .TA,
                             degree : "Master",
                             hobbies : ["Computer Vision projects", "Tennis", "Traveling"],
                             programmingLanguage : [])

let testPerson5 = DukePerson(firstName: "Wrong",
                             lastName : "Hobby",
                             whereFrom : "Central New Jersey",
                             gender : .Male,
                             role : .TA,
                             degree : "Master",
                             hobbies : ["Computer Vision projects", "Tennis", "Traveling", "Hobby"],
                             programmingLanguage : ["Swift", "Python", "Java"])
let Input = [testPerson1, testPerson2, testPerson3, testPerson4, testPerson5]
let space_joiner = " "

//read input data
var index = 0
var Persons = [DukePerson]()
while (index < Input.count) {
    Persons.append(Input[index])
    index += 1
}
//Use dictionary to store the persons. The limit is that this struction cannot resovle person with the same name.
var personList = [String:DukePerson]()
for person in Persons {
    var name = person.firstName + " " + person.lastName
    personList[name] = person
}

func whoIs(_ name: String) -> String {
    var info = [String]()
    var heOrShe = "He"
    var hobby = ""
    var pLanguage = ""
    var invalidHobby = false
    var invalidLanguage = false
    //make sure if person exists
    if let person = personList[name] {
        if (person.gender == .Female){
            heOrShe = "She"
        }
        info = [name, "is from", person.whereFrom, "and is a", person.role.rawValue + "."]

        //connect programming languages
        switch person.programmingLanguage.count {
        case 0:
            pLanguage = ""
        case 1:
            pLanguage = person.programmingLanguage[0]
        case 2:
            pLanguage = person.programmingLanguage[0] + " and " + person.programmingLanguage[1]
        case 3:
            pLanguage = person.programmingLanguage[0] + ", " + person.programmingLanguage[1] + " and " + person.programmingLanguage[2]
        default :
            invalidLanguage = true
        }
        if (invalidLanguage) {
            return "Error: The number of preferred programming language for this person is not in the range of 1 to 3."
        }
        
        if pLanguage != "" {
            info += [heOrShe, "is proficient in", pLanguage + "."]
        }
        
        //connect hobbies
        switch person.hobbies.count {
        case 0:
            hobby = ""
        case 1:
            hobby = person.hobbies[0]
        case 2:
            hobby = person.hobbies[0] + " and " + person.hobbies[1]
        case 3:
            hobby = person.hobbies[0] + ", " + person.hobbies[1] + " and " + person.hobbies[2]
        default :
            invalidHobby = true
        }
        
        if (invalidHobby) {
            return "Error: The number of hobbies for this person is not in the range of 1 to 3."
        }
        if hobby != "" {
            info += ["When not in class,", person.firstName, "enjoys", hobby + "."]
        }
        //return the introduction
        return info.joined(separator: space_joiner)
    }else {
        return "The person does not exist."
    }
    
}

//tests for whoIs
func testWhoIsRic() -> Bool {
    let ans = "Ric Telford is from Morrisville, NC and is a Professor. He is proficient in Swift, C and C++. When not in class, Ric enjoys Biking, Hiking and Golf."
    return whoIs("Ric Telford") == ans
}

func testWhoIsNonExist() -> Bool {
    let ans = "The person does not exist."
    return whoIs("Ric") == ans
}

func testWhoIsNoLanguage() -> Bool {
    let ans = "No Language is from Central New Jersey and is a Teaching Assistant. When not in class, No enjoys Computer Vision projects, Tennis and Traveling."
    return whoIs("No Language") == ans
}

func testWhoIsInvalidHobby() -> Bool {
    let ans = "Error: The number of hobbies for this person is not in the range of 1 to 3."
    return whoIs("Wrong Hobby") == ans
}

//: **tests by Menglin**
//print ("\(testWhoIsRic())")
//print ("\(testWhoIsNonExist())")
//print ("\(testWhoIsNoLanguage())")
//print ("\(testWhoIsInvalidHobby())")

//print(whoIs("Menglin Liu"))
//print(whoIs("Ric Telford"))
//print(whoIs("Niral Shah"))

//: **tests end**
//: ### END OF HOMEWORK
//: Uncomment the line below to test your homework.
//: The "whoIs" function should be defined as `func whoIs(_ name: String) -> String {   }`

// print(whoIs("Ric Telford"))





