//
//  ViewController.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Ric Telford on 7/16/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    let textFont = UIFont(name: "Helvetica Neue", size: 13)
    let titleFont = UIFont(name: "Bodoni 72 OldStyle Neue", size: 22)
    let sloganFont = UIFont(name: "Chalkduster", size: 18)
    let fieldFont = UIFont(name: "Helvetica Neue", size: 11)
    let textHeight = 25
    let textWidth = 100
    let startPos1 = 15
    let startY = 45
    let startYCG : CGFloat = 45
    let startPos2 : CGFloat = 120
    let fieldHeight : CGFloat = 25
    let mainC = UIColor(red: CGFloat (245/255.0), green: CGFloat (245/255.0) , blue: CGFloat(245/255.0), alpha: 1)
    let keyFieldC = UIColor(red: CGFloat (210/255.0), green: CGFloat (227/255.0) , blue: CGFloat(235/255.0), alpha: 1)
    let buttonC = UIColor(red: CGFloat (102/255.0), green: CGFloat (154/255.0) , blue: CGFloat(204/255.0), alpha: 0.5)
    let buttonBC = UIColor(red: CGFloat (102/255.0), green: CGFloat (154/255.0) , blue: CGFloat(204/255.0), alpha: 1).cgColor
    let subTitleC = UIColor(red: CGFloat (11/255.0), green: CGFloat (40/255.0) , blue: CGFloat(77/255.0), alpha: 1)
    let titleC = UIColor(red: CGFloat (68/255.0), green: CGFloat (129/255.0) , blue: CGFloat(196/255.0), alpha: 1)
    let warnC = UIColor(red: CGFloat (203/255.0), green: CGFloat (0/255.0) , blue: CGFloat(25/255.0), alpha: 1)
    
    var InformationView: UIView!
    var titleView: UILabel!
    var keyView: UIView!
    var keyTitle: UILabel!
    var infoTitle: UILabel!
    
    var firstText: UILabel!
    var lastText: UILabel!
    var firstField: UITextField!
    var lastField: UITextField!
    
    var genderText: UILabel!
    var roleText: UILabel!
    var fromText: UILabel!
    var degreeText: UILabel!
    var hobby1Text: UILabel!
    var hobby2Text: UILabel!
    var hobby3Text: UILabel!
    var lang1Text: UILabel!
    var lang2Text: UILabel!
    var lang3Text: UILabel!
    
    var genderField: UITextField!
    var roleField: UITextField!
    var fromField: UITextField!
    var degreeField: UITextField!
    var hobby1Field: UITextField!
    var hobby2Field: UITextField!
    var hobby3Field: UITextField!
    var lang1Field: UITextField!
    var lang2Field: UITextField!
    var lang3Field: UITextField!
    
    var sliderView: UISlider!
    var addButton: UIButton!
    var clearButton: UIButton!
    var searchButton: UIButton!
    var insLabel: UILabel!
    var resultLabel: UILabel!
    
    var dataModel = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InformationView = self.view
        InformationView.backgroundColor = mainC
        
        //Title
        titleView = UILabel(frame: CGRect(x: 10, y: startYCG-30, width: InformationView.bounds.width-80, height: 60))
        titleView.font = sloganFont
        titleView.text = "Duke Person Management System"
        titleView.textColor = titleC
        titleView.numberOfLines = 0
        titleView.lineBreakMode = NSLineBreakMode.byWordWrapping
        InformationView.addSubview(titleView)
        
        //Subtitle
        keyTitle = UILabel(frame: CGRect(x: 10, y: startYCG+15, width: InformationView.bounds.width-20, height: 40))
        keyTitle.font = titleFont
        keyTitle.text = "Identifier of Duke Person"
        keyTitle.textColor = subTitleC
        InformationView.addSubview(keyTitle)
        
        //view of Key
        keyView = UIView(frame: CGRect(x: 10, y: startYCG+45, width: InformationView.bounds.width-20, height: 65))
        keyView.backgroundColor = keyFieldC
        keyView.layer.cornerRadius = 5
        keyTitle.layer.borderColor = UIColor.darkGray.cgColor
        InformationView.addSubview(keyView)
        
        //fistName
        firstText = UILabel(frame: CGRect(x: startPos1, y: startY+50, width: textWidth, height: textHeight))
        firstText.backgroundColor = keyFieldC
        firstText.font = textFont
        firstText.text = "First Name:* "
        InformationView.addSubview(firstText)
        
        firstField = UITextField(frame: CGRect(x: startPos2, y: startYCG+50, width: InformationView.bounds.width-140, height: fieldHeight))
        firstField.backgroundColor = UIColor.white
        firstField.layer.cornerRadius = 2
        firstField.layer.borderColor = buttonBC
        firstField.placeholder = "First Name"
        InformationView.addSubview(firstField)
        
        //LastName
        lastText = UILabel(frame: CGRect(x: startPos1, y: startY+textHeight+55, width: textWidth, height: textHeight))
        lastText.backgroundColor = keyFieldC
        lastText.font = textFont
        lastText.text = "Last Name:* "
        InformationView.addSubview(lastText)
        
        lastField = UITextField(frame: CGRect(x: startPos2, y: startYCG+fieldHeight+55, width: InformationView.bounds.width-140, height: fieldHeight))
        lastField.backgroundColor = UIColor.white
        lastField.layer.cornerRadius = 2
        lastField.layer.borderColor = buttonBC
        lastField.placeholder = "Last Name"
        InformationView.addSubview(lastField)
        
        //Subtitle2
        infoTitle = UILabel(frame: CGRect(x: 10, y: startYCG+105, width: InformationView.bounds.width-20, height: 40))
        infoTitle.font = titleFont
        infoTitle.textColor = subTitleC
        infoTitle.text = "Infomation of Duke Person"
        InformationView.addSubview(infoTitle)
        
        //gender
        genderText = UILabel(frame: CGRect(x: startPos1, y: startY+135, width: textWidth, height: textHeight))
        genderText.font = textFont
        genderText.text = "Gender:* "
        InformationView.addSubview(genderText)
        
        genderField = UITextField(frame: CGRect(x: startPos2, y: startYCG+135, width: InformationView.bounds.width-140, height: fieldHeight))
        genderField.layer.borderWidth = 1
        genderField.layer.borderColor = buttonBC
        genderField.backgroundColor = UIColor.white
        genderField.layer.cornerRadius = 2
        genderField.attributedPlaceholder = NSAttributedString(string: "Male or Female", attributes: [NSFontAttributeName : fieldFont!])
        InformationView.addSubview(genderField)
        
        //role
        roleText = UILabel(frame: CGRect(x: startPos1, y: startY+textHeight+136, width: textWidth, height: textHeight))
        roleText.font = textFont
        roleText.text = "Role:* "
        InformationView.addSubview(roleText)
        
        roleField = UITextField(frame: CGRect(x: startPos2, y: startYCG+fieldHeight+136, width: InformationView.bounds.width-140, height: fieldHeight))
        roleField.layer.borderWidth = 1
        roleField.layer.borderColor = buttonBC
        roleField.backgroundColor = UIColor.white
        roleField.layer.cornerRadius = 2
        roleField.attributedPlaceholder = NSAttributedString(string: "Student, Professor or TA", attributes: [NSFontAttributeName : fieldFont!])
        InformationView.addSubview(roleField)
        
        //whrer to from
        fromText = UILabel(frame: CGRect(x: startPos1, y: startY+2*(textHeight+1)+135, width: textWidth, height: textHeight))
        fromText.font = textFont
        fromText.text = "From:* "
        InformationView.addSubview(fromText)
        
        fromField = UITextField(frame: CGRect(x: startPos2, y: startYCG+2*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        fromField.layer.borderWidth = 1
        fromField.layer.borderColor = buttonBC
        fromField.backgroundColor = UIColor.white
        fromField.layer.cornerRadius = 2
        fromField.attributedPlaceholder = NSAttributedString(string: "Any string of location info", attributes: [NSFontAttributeName : fieldFont!])
        InformationView.addSubview(fromField)
        
        //degree
        degreeText = UILabel(frame: CGRect(x: startPos1, y: startY+3*(textHeight+1)+135, width: textWidth, height: textHeight))
        degreeText.font = textFont
        degreeText.text = "Degree:* "
        InformationView.addSubview(degreeText)
        
        degreeField = UITextField(frame: CGRect(x: startPos2, y: startYCG+3*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        degreeField.layer.borderWidth = 1
        degreeField.layer.borderColor = buttonBC
        degreeField.attributedPlaceholder = NSAttributedString(string: "Any string of degree info", attributes: [NSFontAttributeName : fieldFont!])
        degreeField.backgroundColor = UIColor.white
        degreeField.layer.cornerRadius = 2
        InformationView.addSubview(degreeField)
        
        // up to 3 hobbies
        hobby1Text = UILabel(frame: CGRect(x: startPos1, y: startY+4*(textHeight+1)+135, width: textWidth, height: textHeight))
        hobby1Text.font = textFont
        hobby1Text.text = "Hobby 1: "
        InformationView.addSubview(hobby1Text)
        
        hobby1Field = UITextField(frame: CGRect(x: startPos2, y: startYCG+4*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        hobby1Field.layer.borderWidth = 1
        hobby1Field.layer.borderColor = buttonBC
        hobby1Field.attributedPlaceholder = NSAttributedString(string: "Any string of a hobby", attributes: [NSFontAttributeName : fieldFont!])
        hobby1Field.backgroundColor = UIColor.white
        hobby1Field.layer.cornerRadius = 2
        InformationView.addSubview(hobby1Field)
        
        hobby2Text = UILabel(frame: CGRect(x: startPos1, y: startY+5*(textHeight+1)+135, width: textWidth, height: textHeight))
        hobby2Text.font = textFont
        hobby2Text.text = "Hobby 2: "
        InformationView.addSubview(hobby2Text)
        
        hobby2Field = UITextField(frame: CGRect(x: startPos2, y: startYCG+5*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        hobby2Field.layer.borderWidth = 1
        hobby2Field.layer.borderColor = buttonBC
        hobby2Field.attributedPlaceholder = NSAttributedString(string: "Any string of a hobby", attributes: [NSFontAttributeName : fieldFont!])
        hobby2Field.backgroundColor = UIColor.white
        hobby2Field.layer.cornerRadius = 2
        InformationView.addSubview(hobby2Field)
        
        hobby3Text = UILabel(frame: CGRect(x: startPos1, y: startY+6*(textHeight+1)+135, width: textWidth, height: textHeight))
        hobby3Text.font = textFont
        hobby3Text.text = "Hobby 3: "
        InformationView.addSubview(hobby3Text)
        
        hobby3Field = UITextField(frame: CGRect(x: startPos2, y: startYCG+6*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        hobby3Field.layer.borderWidth = 1
        hobby3Field.layer.borderColor = buttonBC
        hobby3Field.attributedPlaceholder = NSAttributedString(string: "Any string of a hobby", attributes: [NSFontAttributeName : fieldFont!])
        hobby3Field.backgroundColor = UIColor.white
        hobby3Field.layer.cornerRadius = 2
        InformationView.addSubview(hobby3Field)
        
        //up to 3 preferred languages
        lang1Text = UILabel(frame: CGRect(x: startPos1, y: startY+7*(textHeight+1)+135, width: textWidth, height: textHeight))
        lang1Text.font = textFont
        lang1Text.text = "Language 1: "
        InformationView.addSubview(lang1Text)
        
        lang1Field = UITextField(frame: CGRect(x: startPos2, y: startYCG+7*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        lang1Field.layer.borderWidth = 1
        lang1Field.layer.borderColor = buttonBC
        lang1Field.attributedPlaceholder = NSAttributedString(string: "Any string of a preferred language", attributes: [NSFontAttributeName : fieldFont!])
        lang1Field.backgroundColor = UIColor.white
        lang1Field.layer.cornerRadius = 2
        InformationView.addSubview(lang1Field)
        
        lang2Text = UILabel(frame: CGRect(x: startPos1, y: startY+8*(textHeight+1)+135, width: textWidth, height: textHeight))
        lang2Text.font = textFont
        lang2Text.text = "Language 2: "
        InformationView.addSubview(lang2Text)
        
        lang2Field = UITextField(frame: CGRect(x: startPos2, y: startYCG+8*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        lang2Field.layer.borderWidth = 1
        lang2Field.layer.borderColor = buttonBC
        lang2Field.attributedPlaceholder = NSAttributedString(string: "Any string of a preferred language", attributes: [NSFontAttributeName : fieldFont!])
        lang2Field.backgroundColor = UIColor.white
        lang2Field.layer.cornerRadius = 2
        InformationView.addSubview(lang2Field)
        
        lang3Text = UILabel(frame: CGRect(x: startPos1, y: startY+9*(textHeight+1)+135, width: textWidth, height: textHeight))
        lang3Text.font = textFont
        lang3Text.text = "Language 3: "
        InformationView.addSubview(lang3Text)
        
        lang3Field = UITextField(frame: CGRect(x: startPos2, y: startYCG+9*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight))
        lang3Field.layer.borderWidth = 1
        lang3Field.layer.borderColor = buttonBC
        lang3Field.attributedPlaceholder = NSAttributedString(string: "Any string of a preferred language", attributes: [NSFontAttributeName : fieldFont!])
        lang3Field.backgroundColor = UIColor.white
        lang3Field.layer.cornerRadius = 2
        InformationView.addSubview(lang3Field)
        
        //add/update button
        addButton = UIButton()
        addButton.backgroundColor = buttonC
        addButton.layer.cornerRadius = 5
        addButton.layer.borderWidth = 1
        addButton.layer.borderColor = buttonBC
        addButton.isHidden = false
        addButton.setTitle("Add/Update", for: .normal)
        addButton.setTitleColor(UIColor.black, for: .normal)
        addButton.setTitleColor(UIColor.gray, for: .selected)
        addButton.setTitleColor(subTitleC, for: .highlighted)
        addButton.frame = CGRect(x: startPos1, y: startY+400, width: 110, height: textHeight)
        addButton.addTarget(self, action: #selector(ViewController.pressAdd(_:)), for: .touchUpInside)
        addButton.showsTouchWhenHighlighted = true
        InformationView.addSubview(addButton)
        
        //clear button
        clearButton = UIButton()
        clearButton.backgroundColor = buttonC
        clearButton.layer.cornerRadius = 5
        clearButton.layer.borderWidth = 1
        clearButton.layer.borderColor = buttonBC
        clearButton.isHidden = false
        clearButton.setTitle("Clear", for: .normal)
        clearButton.setTitleColor(UIColor.black, for: .normal)
        clearButton.setTitleColor(UIColor.gray, for: .selected)
        clearButton.setTitleColor(subTitleC, for: .highlighted)
        clearButton.frame = CGRect(x: startPos1+118, y: startY+400, width: 70, height: textHeight)
        clearButton.addTarget(self, action: #selector(ViewController.pressClear(_:)), for: .touchUpInside)
        clearButton.showsTouchWhenHighlighted = true
        InformationView.addSubview(clearButton)
        
        //search button
        searchButton = UIButton()
        
        searchButton.backgroundColor = buttonC
        searchButton.layer.cornerRadius = 5
        searchButton.layer.borderWidth = 1
        searchButton.layer.borderColor = buttonBC
        searchButton.isHidden = false
        searchButton.setTitle("Search", for: .normal)
        searchButton.setTitleColor(UIColor.black, for: .normal)
        searchButton.setTitleColor(UIColor.gray, for: .selected)
        searchButton.setTitleColor(subTitleC, for: .highlighted)
        searchButton.frame = CGRect(x: startPos1+196, y: startY+400, width: 90, height: textHeight)
        searchButton.addTarget(self, action: #selector(ViewController.pressSearch(_:)), for: .touchUpInside)
        searchButton.showsTouchWhenHighlighted = true
        InformationView.addSubview(searchButton)
        
        //instruction
        insLabel = UILabel(frame: CGRect(x: 11, y: startYCG+425, width: InformationView.bounds.width-20, height: 20))
        insLabel.text = "Field marked with * is required for adding/updating persons."
        insLabel.font = UIFont(name: "Hiragino Sans", size: 10)
        insLabel.textColor = warnC
        InformationView.addSubview(insLabel)
        
        //result view
        resultLabel = UILabel(frame: CGRect(x: 10, y: startYCG+443, width: InformationView.bounds.width-20, height: 75))
        resultLabel.backgroundColor = keyFieldC
        resultLabel.layer.masksToBounds = true
        resultLabel.layer.cornerRadius = 5
        resultLabel.layer.borderWidth = 1
        resultLabel.layer.borderColor = buttonBC
        resultLabel.font = textFont
        resultLabel.allowsDefaultTighteningForTruncation = true
        resultLabel.adjustsFontSizeToFitWidth = true
        resultLabel.adjustsFontForContentSizeCategory = true
        resultLabel.numberOfLines = 0
        resultLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        resultLabel.textAlignment = NSTextAlignment.center
        resultLabel.minimumScaleFactor = CGFloat(0.1)
        resultLabel.text = ""
        resultLabel.isHidden = true
        InformationView.addSubview(resultLabel)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //reset text field view
    func originalColor(){
        firstText.textColor = UIColor.black
        lastText.textColor = UIColor.black
        genderText.textColor = UIColor.black
        roleText.textColor = UIColor.black
        fromText.textColor = UIColor.black
        degreeText.textColor = UIColor.black
    }
    
    // press add button
    func pressAdd(_ sender: UIButton!) {
        var result : String
        
        originalColor()
        if (firstField.text=="")||(lastField.text=="")||(genderField.text=="")||(roleField.text=="")||(fromField.text=="")||(degreeField.text=="") {
            if (firstField.text==""){firstText.textColor = warnC}
            if (lastField.text=="") {lastText.textColor = warnC}
            if (genderField.text==""){genderText.textColor = warnC}
            if (roleField.text==""){roleText.textColor = warnC}
            if (fromField.text=="") {fromText.textColor = warnC}
            if (degreeField.text==""){degreeText.textColor = warnC}
            
            resultLabel.text = "Error: Required field(s) must be filled!"
            resultLabel.isHidden = false
            return
        }
        
        if ((genderField.text != "Male") && (genderField.text != "Female"))||((roleField.text != "Professor") && (roleField.text != "TA") && (roleField.text != "Teaching Assistant") && (roleField.text != "Student")) {
            if ((genderField.text != "Male") && (genderField.text != "Female")){genderText.textColor = warnC}
            if ((roleField.text != "Professor") && (roleField.text != "TA") && (roleField.text != "Teaching Assistant") && (roleField.text != "Student")){
                roleText.textColor = warnC
            }
            resultLabel.text = "Error: Invalid input for required field(s)!"
            resultLabel.isHidden = false
            return
        }
        
        var newGender = Gender.Male
        var newRole = DukeRole.Student
        if genderField.text=="Female" {
            newGender = Gender.Female
        }
        if (roleField.text == "TA") || (roleField.text == "Teaching Assistant") {
            newRole = DukeRole.TA
        }
        if roleField.text == "Professor" {
            newRole = DukeRole.Professor
        }
        var newHobby = [String]()
        if ((hobby1Field.text) != ""){
            newHobby.append(hobby1Field.text!)}
        if ((hobby2Field.text) != ""){
            newHobby.append(hobby2Field.text!)}
        if ((hobby3Field.text) != ""){
            newHobby.append(hobby3Field.text!)}
        var newLang = [String]()
        if ((lang1Field.text) != ""){
            newLang.append(lang1Field.text!)}
        if ((lang2Field.text) != ""){
            newLang.append(lang2Field.text!)}
        if ((lang3Field.text) != ""){
            newLang.append(lang3Field.text!)}
        let newPerson = DukePerson(firstName: firstField.text!,
                                   lastName : lastField.text!,
                                   whereFrom : fromField.text!,
                                   gender : newGender,
                                   role : newRole,
                                   degree : degreeField.text!,
                                   hobbies : newHobby,
                                   programmingLanguage : newLang,
                                   teamName : "")
        
        result = firstField.text!+" "+lastField.text!
        let newName = newPerson.firstName + " " + newPerson.lastName
        //determine wether add or update
        if dataModel.getPerson(newName) != nil {
            resultLabel.text = result + " is updated successfully!"
        }else{
            resultLabel.text = result + " is aded successfully!"
        }
        dataModel.addPerson(newName, newDukePerson: newPerson)
        resultLabel.isHidden = false
    }
    
    //press clear button
    func pressClear(_ sender: UIButton!) {
        originalColor()
        firstField.text?=""
        lastField.text?=""
        genderField.text?=""
        roleField.text?=""
        fromField.text?=""
        degreeField.text?=""
        hobby1Field.text?=""
        hobby2Field.text?=""
        hobby3Field.text?=""
        lang1Field?.text?=""
        lang2Field.text?=""
        lang3Field.text?=""
        resultLabel.isHidden = true
        
    }
    
    //press search button
    func pressSearch(_ sender: UIButton!) {
        originalColor()
        var result: String
        if (firstField.text=="")||(lastField.text=="") {
            if (firstField.text==""){firstText.textColor = warnC}
            if (lastField.text=="") {lastText.textColor = warnC}
            
            resultLabel.text = "Error: Required field(s) must be filled!"
            resultLabel.isHidden = false
            return
        }
        hobby1Field.text?=""
        hobby2Field.text?=""
        hobby3Field.text?=""
        lang1Field?.text?=""
        lang2Field.text?=""
        lang3Field.text?=""
        result = dataModel.whoIs(firstField.text!+" "+lastField.text!)
        if (result != "The person does not exist.") {
            let thisPerson = dataModel.getPerson(firstField.text!+" "+lastField.text!)
            firstField.text?=(thisPerson?.firstName)!
            lastField.text?=(thisPerson?.lastName)!
            genderField.text?="Male"
            if (thisPerson?.gender == Gender.Female) {
                genderField.text?="Female"
            }
            roleField.text?="Teaching Assistant"
            if (thisPerson?.role == DukeRole.Student) {
                roleField.text?="Student"
            }
            if (thisPerson?.role == DukeRole.Professor) {
                roleField.text?="Professor"
            }
            fromField.text?=(thisPerson?.whereFrom)!
            degreeField.text?=(thisPerson?.degree)!
            if ((thisPerson?.hobbies.count)! >= 1) {
                hobby1Field.text?=(thisPerson?.hobbies[0])!}
            if ((thisPerson?.hobbies.count)! >= 2) {
                hobby2Field.text?=(thisPerson?.hobbies[1])!}
            if ((thisPerson?.hobbies.count)! >= 3) {
                hobby3Field.text?=(thisPerson?.hobbies[2])!}
            if ((thisPerson?.programmingLanguage.count)! >= 1) {
                lang1Field.text?=(thisPerson?.programmingLanguage[0])!}
            if ((thisPerson?.programmingLanguage.count)! >= 2) {
                lang2Field.text?=(thisPerson?.programmingLanguage[1])!
            }
            if ((thisPerson?.programmingLanguage.count)! >= 3) {
                lang3Field.text?=(thisPerson?.programmingLanguage[2])!}
        }
        resultLabel.isHidden = false
        resultLabel.text=result
    }
    
}

