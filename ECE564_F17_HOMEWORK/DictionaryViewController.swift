//
//  DictionaryViewController.swift
//  ECE564_F17_HOMEWORK
//
//  Created by 刘孟琳 on 9/18/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit

class DictionaryViewController: UITableViewController {
        
    var professorList = [DukePerson]()
    var TAList = [DukePerson]()
    var StudentList = [DukePerson]()
    var dataModel = DataModel()

    let imageProfile = "profile-pictures.png"
    let textFont = UIFont(name: "Helvetica Neue", size: 10)
    
    @IBAction func returnFromNewItem(segue: UIStoryboardSegue) {
        let source: NewPersonViewController = segue.source as! NewPersonViewController
        let person3: DukePerson = source.newPerson
        if (person3.firstName != "Missing Value"){
            self.dataModel.addPerson(person3.firstName + " " + person3.lastName, newDukePerson: person3)
            self.saveData()
        }
        self.tableView.reloadData()
        
    }
    
    @IBAction func returnFromSearch(segue: UIStoryboardSegue) {}
    
    @IBAction func returnFromUpdatePerson(segue: UIStoryboardSegue) {
        self.saveData()
        self.loadData()
    }
    
    var filePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        //print("this is the url path in the documentDirectory \(url)")
        return (url!.appendingPathComponent("Data").path)
    }
    
    func saveData() {
        NSKeyedArchiver.archiveRootObject(self.dataModel.Persons, toFile: filePath)

    }
    
    func loadData() {
        if let ourData = NSKeyedUnarchiver.unarchiveObject(withFile: filePath) as? [DukePerson] {
            self.dataModel.reload(newPersonList: ourData)
        }
    }
    
    private func clearData(){
        self.dataModel.clearAll()
        NSKeyedArchiver.archiveRootObject(self.dataModel.Persons, toFile: filePath)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //start from here 
        //self.saveData()
        self.loadData()
    }
    override func tableView (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessorSep", for: indexPath)
            cell.textLabel?.text = "Professor"
            return cell
        case 1:
            let index = indexPath.row
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessorList", for: indexPath)
            let tempPerson: DukePerson = self.dataModel.professors[index]
            cell.textLabel?.text = tempPerson.firstName + " " + tempPerson.lastName + "\n" + tempPerson.description
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.textLabel?.font = textFont
            cell.textLabel?.allowsDefaultTighteningForTruncation = true
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            cell.textLabel?.adjustsFontForContentSizeCategory = true
            let image = UIImage(named: imageProfile)
            cell.imageView!.image = image
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TASep", for: indexPath)
            cell.textLabel?.text = "TA"
            return cell
        case 3:
            let index = indexPath.row
            let cell = tableView.dequeueReusableCell(withIdentifier: "TAList", for: indexPath)
            let tempPerson: DukePerson = self.dataModel.TAs[index]
            cell.textLabel?.text = tempPerson.firstName + " " + tempPerson.lastName + "\n" +
                tempPerson.description
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.textLabel?.font = textFont
            cell.textLabel?.allowsDefaultTighteningForTruncation = true
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            cell.textLabel?.adjustsFontForContentSizeCategory = true
            let image = UIImage(named: imageProfile)
            cell.imageView!.image = image
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TASep", for: indexPath)
            cell.textLabel?.text = "Student"
            return cell
        case 5:
            let index = indexPath.row
            let cell = tableView.dequeueReusableCell(withIdentifier: "StudentList", for: indexPath)
            let tempPerson: DukePerson = self.dataModel.students[index]
            cell.textLabel?.text = tempPerson.firstName + " " + tempPerson.lastName + "\n" +
                tempPerson.description
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell.textLabel?.font = textFont
            cell.textLabel?.allowsDefaultTighteningForTruncation = true
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            cell.textLabel?.adjustsFontForContentSizeCategory = true
            let image = UIImage(named: imageProfile)
            cell.imageView!.image = image
            return cell
            
        default:
            var index = indexPath.row
            var teamCount = 0;
            for teamPair in self.dataModel.teamList{
                if index == teamCount {
                    let cell = UITableViewCell()
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "StudentSep", for: indexPath)
                    cell.textLabel?.text = teamPair.key
                    return cell
                }else{
                    teamCount = teamCount + teamPair.value.count + 1
                    if (index < teamCount) {
                        teamCount = teamCount - teamPair.value.count - 1
                        index = index - teamCount - 1
                        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamList", for: indexPath)
                        let tempPerson: DukePerson = teamPair.value[index]
                        cell.textLabel?.text = tempPerson.firstName + " " + tempPerson.lastName + "\n" +
                            tempPerson.description
                        cell.textLabel?.numberOfLines = 0
                        cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                        cell.textLabel?.font = textFont
                        cell.textLabel?.allowsDefaultTighteningForTruncation = true
                        cell.textLabel?.adjustsFontSizeToFitWidth = true
                        cell.textLabel?.adjustsFontForContentSizeCategory = true
                        let image = UIImage(named: imageProfile)
                        cell.imageView!.image = image
                        return cell
                    }
                }
            }
            let cell = UITableViewCell()
                //tableView.dequeueReusableCell(withIdentifier: "StudentSep", for: indexPath)
            cell.textLabel?.text = "Error"
            return cell
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ((indexPath.section == 0) || (indexPath.section == 2) || (indexPath.section == 4)) {
            return
        }
        performSegue(withIdentifier: "profileDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SearchPerson") {
            let SearchPerson = segue.destination as! SearchViewController
            SearchPerson.filePath = self.filePath
            return
        }
        if (segue.identifier != "profileDetail") {
            return
        }
        let indexPath = tableView.indexPathForSelectedRow
        if ((indexPath?.section == 0) || (indexPath?.section == 2) || (indexPath?.section == 4)) {
            return
        }
        let UpdatePerson = segue.destination as! UpdatePerson
        var templist = [DukePerson]()
        switch (indexPath!.section) {
        case 1:
            templist = self.dataModel.professors
            UpdatePerson.thisPerson = templist[indexPath!.row]
        case 3:
            templist = self.dataModel.TAs
            UpdatePerson.thisPerson = templist[indexPath!.row]
        case 5:
            templist = self.dataModel.students
            UpdatePerson.thisPerson = templist[indexPath!.row]
        default:
            var teamCount = 0
            for teamPair in self.dataModel.teamList {
                teamCount = teamCount + 1
                for eachPerson in teamPair.value {
                    if (indexPath?.row == teamCount) {
                        UpdatePerson.thisPerson = eachPerson
                    }
                    teamCount = teamCount + 1
                }
            }
        }
        
        UpdatePerson.previousSection = (indexPath?.section)!
        UpdatePerson.previousRow = (indexPath?.row)!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        if (self.dataModel.teamList.isEmpty){
            return 6
        }else{
            return 7
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return 1
        case 1:
            return self.dataModel.professors.count
        case 2:
            return 1
        case 3:
            return self.dataModel.TAs.count
        case 4:
            return 1
        case 5:
            return self.dataModel.students.count
        default:
            var teamCount = self.dataModel.teamList.count
            for teamPair in self.dataModel.teamList{
                teamCount = teamCount + teamPair.value.count
            }
            return teamCount
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        let row = indexPath.row
        if ((section == 0) || (section == 2) || (section == 4)) {
            return 40
        }
        if (section == 6) {
            var teamCount = 0
            for teamPair in self.dataModel.teamList {
                if (row == teamCount) {
                    return 40
                }
                teamCount = teamCount + teamPair.value.count + 1
            }
        }
        return 100
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if ((indexPath.section == 0) || (indexPath.section == 2) || (indexPath.section == 4)) {
                return
            }
            var removePerson : DukePerson!
            var templist = [DukePerson]()
            switch (indexPath.section) {
            case 1:
                templist = self.dataModel.professors
                removePerson = templist[indexPath.row]
            case 3:
                templist = self.dataModel.TAs
                removePerson = templist[indexPath.row]
            case 5:
                templist = self.dataModel.students
                removePerson = templist[indexPath.row]
            default:
                var teamCount = 0
                for teamPair in self.dataModel.teamList {
                    if (indexPath.row == teamCount) {
                        return
                    }
                    teamCount = teamCount + 1
                    for eachPerson in teamPair.value {
                        if (indexPath.row == teamCount) {
                            removePerson = eachPerson
                        }
                        teamCount = teamCount + 1
                    }
                }
            }
            if (indexPath.section != 6){
            if removePerson != nil {
                    self.dataModel.removePerson(willRemove: removePerson)
                    self.saveData()
                    self.loadData()
                }
                tableView.deleteRows(at: [indexPath], with: .fade)
            }else{
                if removePerson != nil {
                    self.dataModel.removePerson(willRemove: removePerson)
                    self.saveData()
                    self.loadData()
                }
                self.tableView.reloadData()
                /*
                if (self.tableView.numberOfRows(inSection: 6) > 2 ) {
                   tableView.deleteRows(at: [indexPath], with: .fade)
                }else{
                 
                }*/
            }
            

        }
    }

}
