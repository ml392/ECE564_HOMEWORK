//
//  DataModel.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Ric Telford on 7/16/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit

class DataModel{
    let testPerson1 = DukePerson(firstName: "Ric",
                                 lastName : "Telford",
                                 whereFrom : "Morrisville, NC",
                                 gender : .Male,
                                 role : .Professor,
                                 degree : "BS",
                                 hobbies : ["Biking" , "Hiking", "Golf"],
                                 programmingLanguage : ["Swift", "C", "C++"],
                                 teamName: "")
    
    let testPerson2 = DukePerson(firstName: "Menglin",
                                 lastName : "Liu",
                                 whereFrom : "Zhengzhou, China",
                                 gender : .Female,
                                 role : .Student,
                                 degree : "Master",
                                 hobbies : ["Singing" , "Painting", "Basketball"],
                                 programmingLanguage : ["C++", "Java"],
                                 teamName: "Fabers")
    
    let testPerson3 = DukePerson(firstName: "Niral",
                                 lastName : "Shah",
                                 whereFrom : "Central New Jersey",
                                 gender : .Male,
                                 role : .TA,
                                 degree : "Master",
                                 hobbies : ["Computer Vision projects", "Tennis", "Traveling"],
                                 programmingLanguage : ["Swift", "Python", "Java"],
                                 teamName: "")
    
    let testPerson4 = DukePerson(firstName: "Gilbert",
                                 lastName : "Brooks",
                                 whereFrom : "Shelby, NC",
                                 gender : .Male,
                                 role : .TA,
                                 degree : "BA",
                                 hobbies : ["User Experience" , "Product Development"],
                                 programmingLanguage : ["Swift", "Java"],
                                 teamName: "")
    
    let testPerson5 = DukePerson(firstName: "Eric",
                                 lastName : "Gossett",
                                 whereFrom : "Nowhere",
                                 gender : .Male,
                                 role : .Student,
                                 degree : "Master",
                                 hobbies : ["Computer Vision projects", "Traveling", "Hobby"],
                                 programmingLanguage : ["Swift", "Python", "Java"],
                                 teamName: "")
    
    var Persons :[DukePerson]
    var personList = [String:DukePerson]()
    var professors = [DukePerson]()
    var students = [DukePerson]()
    var TAs = [DukePerson]()
    var teamList = [String: [DukePerson]]()
    init(){
        Persons = [testPerson1, testPerson3, testPerson4, testPerson5]
        for person in Persons {
            let name = person.firstName + " " + person.lastName
            personList[name] = person
        }
        professors = [testPerson1]
        TAs = [testPerson3, testPerson4]
        students = [testPerson5]
        addPerson("Menglin Liu", newDukePerson: testPerson2)
    }
    
    func whoIs(_ name: String) -> String {
        //make sure if person exists
        if let person = personList[name] {
            return person.description
            //return the introduction
        }else {
            return "The person does not exist."
        }
    }
    
    func getTeam(_ name: String) -> [DukePerson]? {
        if let team = teamList[name] {
            return team
        }
        return nil
    }
    
    func getPerson(_ name: String) -> DukePerson? {
        if let person = personList[name] {
            return person
        }
        return nil
    }
    
    func addPerson(_ name: String, newDukePerson: DukePerson){
        personList[name] = newDukePerson
        Persons.append(newDukePerson)
        if (newDukePerson.role == DukeRole.Professor) {
            professors.append(newDukePerson)
        }else{
            if (newDukePerson.role == DukeRole.TA) {
                TAs.append(newDukePerson)
            }else{
                if (newDukePerson.teamName == ""){
                    students.append(newDukePerson)
                }else{
                    
                    if (self.getTeam(newDukePerson.teamName) != nil) {
                        teamList[newDukePerson.teamName]!.append(newDukePerson)
                    }else{
                        teamList[newDukePerson.teamName] = [newDukePerson]
                    }
                }
            }
        }
    }
    func updatePerson(_ newPerson: DukePerson, willRemove: DukePerson, index: Int){
        //update persons
        var tempIndex = 0
        for eachPerson in Persons {
            if (eachPerson == willRemove) {
                Persons[tempIndex] = newPerson
            }
            tempIndex = tempIndex + 1
        }
        switch (newPerson.role) {
        case DukeRole.Professor:
            professors[index] = newPerson
        case DukeRole.TA:
            TAs[index] = newPerson
        default:
            if (newPerson.teamName == ""){
                students[index] = newPerson
            }else{
                var tempIndex = 0
                for eachPerson in teamList[newPerson.teamName]! {
                    if (eachPerson == willRemove) {
                        teamList[newPerson.teamName]?[tempIndex] = newPerson
                    }
                    tempIndex = tempIndex + 1
                }
            }
        }
    }
    
    func removePerson (willRemove: DukePerson){
        var index = 0
        for eachPerson in Persons {
            if ((eachPerson.firstName == willRemove.firstName) && (eachPerson.lastName == willRemove.lastName)) {
                Persons.remove(at: index)
            }
            index = index + 1
        }
    }
    
    func clearAll() {
        self.teamList.removeAll()
        self.personList.removeAll()
        self.students.removeAll();
        self.Persons.removeAll()
        self.professors.removeAll()
        self.TAs.removeAll()
    }
    
    func reload(newPersonList: [DukePerson]!)  {
        self.clearAll()
        for thisPerson in newPersonList {
            self.addPerson(thisPerson.firstName + " " + thisPerson.lastName, newDukePerson: thisPerson)
        }
    }
}
