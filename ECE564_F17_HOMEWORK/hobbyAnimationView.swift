//
//  hobbyAnimationView.swift
//  ECE564_F17_HOMEWORK
//
//  Created by 刘孟琳 on 9/27/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit

class hobbyAnimationView: UIView {
    override func draw(_ rect: CGRect) {
        
        let h = frame.height
        let w = frame.width
        let color:UIColor = UIColor.black
        color.set()
        //draw circle1
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: w * 0.5,y: h * 0.5-10), radius: CGFloat(45), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        circlePath.stroke()
        //draw circle2
        let scirclePath = UIBezierPath(arcCenter: CGPoint(x: w * 0.5,y: h * 0.5-10), radius: CGFloat(40), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        scirclePath.stroke()
        //draw circle3
        let centerPath = UIBezierPath(arcCenter: CGPoint(x: w * 0.5,y: h * 0.5-10), radius: CGFloat(10), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        centerPath.stroke()
        //draw base
        let lineContext = UIGraphicsGetCurrentContext()
        lineContext?.move(to: CGPoint(x: w * 0.5,y: h * 0.5-10))
        lineContext?.addLine(to: CGPoint(x: w * 0.5 - 15,y: h * 0.5+50))
        lineContext?.addLine(to: CGPoint(x: w * 0.5 + 15,y: h * 0.5+53))
        lineContext?.addLine(to: CGPoint(x: w * 0.5,y: h * 0.5-10))
        //draw lines
        lineContext?.addLines(between: [CGPoint(x: w * 0.5 - 50, y: h * 0.5-10),CGPoint(x: w * 0.5 + 50, y: h * 0.5-10)])
        lineContext?.addLines(between: [CGPoint(x: w * 0.5, y: h * 0.5-60),CGPoint(x: w * 0.5, y: h * 0.5+40)])
        lineContext?.addLines(between: [CGPoint(x: w * 0.5 - 40, y: h * 0.5-50),CGPoint(x: w * 0.5 + 40, y: h * 0.5+30)])
        lineContext?.addLines(between: [CGPoint(x: w * 0.5 - 40, y: h * 0.5+30),CGPoint(x: w * 0.5 + 40, y: h * 0.5-50)])
        lineContext?.strokePath()
        
    }
 

}
