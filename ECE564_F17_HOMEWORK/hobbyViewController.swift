//
//  hobbyViewController.swift
//  ECE564_F17_HOMEWORK
//
//  Created by 刘孟琳 on 9/27/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit
import AVFoundation


class hobbyViewController : UIViewController {
    
    @IBOutlet weak var flipBack: UIButton!
    let imagesky1 = "sky1.jpg"
    let imagesky2 = "sky2.jpg"
    let imagewheel = "wheelonly.png"
    let imagebase = "ferris.png"
    let imageclouds = "clouds.png"
    let imagecloud1 = "cloudl.png"
    let imagecloud2 = "cloudr.png"
    let imagecloud3 = "could3.png"
    let imagebrush = "brush.png"
    let imagecanvas = "canvas.png"
    
    var mainView : UIView!
    var rootLayer : CALayer!
    var wheelLayer: CALayer!
    var ferrisLayer: CALayer!
    var cloudLayer: CALayer!
    var cloudsLayer: CALayer!
    var cloud1Layer: CALayer!
    var cloud2Layer: CALayer!
    var cloud3Layer: CALayer!
    var cloud4Layer: CALayer!
    var brushLayer: CALayer!
    var canvasLayer: CALayer!
    var paintView: hobbyAnimationView!
    var coverView: UIButton!
    let pianoSound = Bundle.main.path(forResource: "Merry-go-round", ofType: "mp3")!
    var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView = self.view
        mainView.backgroundColor = UIColor(red: CGFloat (25/255.0), green: CGFloat (18/255.0) , blue: CGFloat(18/255.0), alpha: 1)
        //play music
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: pianoSound))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
            audioPlayer.play()

        }
        catch{
            print(error)
        }
        
        //sky back
        rootLayer = CALayer()
        rootLayer.frame = CGRect(x: 0.0, y: 0.0, width: mainView.bounds.width, height: mainView.bounds.height-300)
        rootLayer.backgroundColor = UIColor.lightGray.cgColor
        rootLayer.contents = UIImage(named: imagesky2)?.cgImage
        mainView.layer.addSublayer(rootLayer)
        
        // ferris layer
        ferrisLayer = CALayer()
        ferrisLayer.frame = CGRect(x: 10, y: 30, width: mainView.bounds.width-70, height: mainView.bounds.width-70)
        ferrisLayer.contents = UIImage(named: imagebase)?.cgImage
        rootLayer.addSublayer(ferrisLayer)
        
        // wheel layer
        wheelLayer = CALayer()
        wheelLayer.frame = CGRect(x: 10, y: 30, width: mainView.bounds.width-70, height: mainView.bounds.width-70)
        //wheelLayer.backgroundColor = UIColor.white.cgColor
        let wheel = UIImage(named: imagewheel)
        wheelLayer.contents = wheel?.cgImage
        rootLayer.addSublayer(wheelLayer) 
        
        // clouds layer
        cloudsLayer = CALayer()
        cloudsLayer.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: 200)
        rootLayer.addSublayer(cloudsLayer)
        
        // clouds layer
        cloud3Layer = CALayer()
        cloud3Layer.frame = CGRect(x: 20, y: 30, width: 120, height: 50)
        cloud3Layer.contents = UIImage(named: imageclouds)?.cgImage
        cloudsLayer.addSublayer(cloud3Layer)
        
        // cloud layer
        cloudLayer = CALayer()
        cloudLayer.frame = CGRect(x: 230, y: 20, width: 60, height: 30)
        cloudLayer.contents = UIImage(named: imagecloud1)?.cgImage
        cloudsLayer.addSublayer(cloudLayer)
        
        // cloud layer
        cloud1Layer = CALayer()
        cloud1Layer.frame = CGRect(x: 150, y: 40, width: 60, height: 30)
        cloud1Layer.contents = UIImage(named: imagecloud2)?.cgImage
        cloudsLayer.addSublayer(cloud1Layer)
        
        // cloud layer
        cloud2Layer = CALayer()
        cloud2Layer.frame = CGRect(x: 210, y: 80, width: 40, height: 20)
        cloud2Layer.contents = UIImage(named: imagecloud2)?.cgImage
        cloudsLayer.addSublayer(cloud2Layer)
        
        // clouds layer
        cloud4Layer = CALayer()
        cloud4Layer.frame = CGRect(x: 280, y: 50, width: 120, height: 50)
        cloud4Layer.contents = UIImage(named: imageclouds)?.cgImage
        cloudsLayer.addSublayer(cloud4Layer)

        
        //canvas
        canvasLayer = CALayer()
        canvasLayer.frame = CGRect(x: 80, y: 280, width: mainView.bounds.width-120, height: 280)
        canvasLayer.contents = UIImage(named: imagecanvas)?.cgImage
        mainView.layer.addSublayer(canvasLayer)
        
        //paint view
        paintView = hobbyAnimationView(frame: CGRect(x: 125, y: 310, width: mainView.bounds.width-180, height: 120))
        paintView.backgroundColor = UIColor.white
        paintView.draw(paintView.frame)
        mainView.addSubview(paintView)
        
        //brush
        brushLayer = CALayer()
        brushLayer.frame = CGRect(x: 150, y: 280, width: 20, height: 55)
        brushLayer.contents = UIImage(named: imagebrush)?.cgImage
        mainView.layer.addSublayer(brushLayer)
        
        //cover
        coverView = UIButton(frame: rootLayer.frame)
        mainView.addSubview(coverView)

        //bring flip to front
        mainView.bringSubview(toFront: flipBack)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        spinningWheel()
        floatClouds()
        moveBrush()
    }
    
    func spinningWheel(){
        let rotationPoint = CGPoint(x: 135.5, y: 150)
        let width = wheelLayer.frame.width
        let height = wheelLayer.frame.height
        let minX = wheelLayer.frame.minX
        let minY = wheelLayer.frame.minY
        
        let anchorPoint = CGPoint(x: (rotationPoint.x-minX)/width,
                                  y: (rotationPoint.y-minY)/height)
        wheelLayer.anchorPoint = anchorPoint;
        wheelLayer.position = rotationPoint;
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(.pi * 90.0)
        rotateAnimation.duration = 1800.0
        wheelLayer.add(rotateAnimation, forKey: "spinning")
    }
    
    func floatClouds(){
        let theAnimation = CABasicAnimation(keyPath: "position");
        theAnimation.fromValue = [145,100]
        theAnimation.toValue = [105, 100]
        theAnimation.duration = 10.0;
        theAnimation.autoreverses = true
        theAnimation.repeatCount = 10
        cloudsLayer.add(theAnimation, forKey: "animatePosition")
    }
    
    func shakeClouds(){
        let theAnimation = CABasicAnimation(keyPath: "transform.scale.x")
        theAnimation.fromValue = 1
        theAnimation.toValue = 1.05
        theAnimation.autoreverses = true
        theAnimation.repeatCount = 60
        cloudsLayer.add(theAnimation, forKey: "large")
    }

    func moveBrush(){
        let brushAnimation = CAKeyframeAnimation()
        brushAnimation.keyPath = "position"
        brushAnimation.values = [[170,300], [155,330], [170,360], [200,380], [230,360], [240,330], [230,300], [200,290],[170,300]]
        brushAnimation.duration = 18.0
        brushAnimation.repeatCount = 10
        brushLayer.add(brushAnimation, forKey: "position")
    }
}
