//
//  NewPersonViewController.swift
//  ECE564_F17_HOMEWORK
//
//  Created by 刘孟琳 on 9/18/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit

class NewPersonViewController: UIViewController {

    @IBOutlet weak var addButton: UIButton!
    
    
    @IBOutlet weak var firstField: UITextField!
    @IBOutlet weak var lastField: UITextField!
    @IBOutlet weak var genderField: UITextField!
    @IBOutlet weak var roleField: UITextField!
    @IBOutlet weak var fromField: UITextField!
    @IBOutlet weak var degreeField: UITextField!
    @IBOutlet weak var hobby1Field: UITextField!
    @IBOutlet weak var hobby2Field: UITextField!
    @IBOutlet weak var hobby3Field: UITextField!
    @IBOutlet weak var lang1Field: UITextField!
    @IBOutlet weak var lang2Field: UITextField!
    @IBOutlet weak var lang3Field: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var teamField: UITextField!
    
    let textFont = UIFont(name: "Helvetica Neue", size: 13)
    let titleFont = UIFont(name: "Bodoni 72 OldStyle Neue", size: 22)
    let sloganFont = UIFont(name: "Chalkduster", size: 18)
    let fieldFont = UIFont(name: "Helvetica Neue", size: 11)
    let textHeight = 25
    let textWidth = 100
    let startPos1 = 15
    let startY = 95
    let startYCG : CGFloat = 95
    let startPos2 : CGFloat = 120
    let fieldHeight : CGFloat = 25
    let imageProfile = "profile-pictures.png"
    let mainC = UIColor(red: CGFloat (245/255.0), green: CGFloat (245/255.0) , blue: CGFloat(245/255.0), alpha: 1)
    let keyFieldC = UIColor(red: CGFloat (210/255.0), green: CGFloat (227/255.0) , blue: CGFloat(235/255.0), alpha: 1)
    let buttonC = UIColor(red: CGFloat (102/255.0), green: CGFloat (154/255.0) , blue: CGFloat(204/255.0), alpha: 0.5)
    let buttonBC = UIColor(red: CGFloat (102/255.0), green: CGFloat (154/255.0) , blue: CGFloat(204/255.0), alpha: 1).cgColor
    let subTitleC = UIColor(red: CGFloat (11/255.0), green: CGFloat (40/255.0) , blue: CGFloat(77/255.0), alpha: 1)
    let titleC = UIColor(red: CGFloat (68/255.0), green: CGFloat (129/255.0) , blue: CGFloat(196/255.0), alpha: 1)
    let warnC = UIColor(red: CGFloat (203/255.0), green: CGFloat (0/255.0) , blue: CGFloat(25/255.0), alpha: 1)
    
    var InformationView: UIView!
    var titleView: UILabel!
    var keyView: UIView!
    var keyTitle: UILabel!
    var infoTitle: UILabel!
    var teamText: UILabel!
    
    var firstText: UILabel!
    var lastText: UILabel!
    var genderText: UILabel!
    var roleText: UILabel!
    var fromText: UILabel!
    var degreeText: UILabel!
    var hobby1Text: UILabel!
    var hobby2Text: UILabel!
    var hobby3Text: UILabel!
    var lang1Text: UILabel!
    var lang2Text: UILabel!
    var lang3Text: UILabel!
    var resultLabel: UILabel!
    
    var sliderView: UISlider!
    var yesButton: UIButton!
    var clearButton: UIButton!
    var insLabel: UILabel!
    var saveButton: UIBarButtonItem!
    
    var dataModel = DataModel()
    var newPerson = DukePerson(firstName: "Missing Value",
                               lastName : "Missing Value",
                               whereFrom : "Missing Value",
                               gender : Gender.Female,
                               role : DukeRole.Student,
                               degree : "Missing Value",
                               hobbies : [],
                               programmingLanguage : [],
                               teamName: "")
    
    @IBAction func returnFromAddPhoto(segue: UIStoryboardSegue) {}
    @IBAction func returnFromNewPhotoView(segue: UIStoryboardSegue) {
        let source: NewPhotoViewController = segue.source as! NewPhotoViewController
        profileImage.image = source.takenPhoto
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InformationView = self.view
        InformationView.backgroundColor = mainC
        
        //Title
        titleView = UILabel(frame: CGRect(x: 10, y: startYCG-35, width: InformationView.bounds.width-80, height: 60))
        titleView.font = sloganFont
        titleView.text = "Duke Person Management System"
        titleView.textColor = titleC
        titleView.numberOfLines = 0
        titleView.lineBreakMode = NSLineBreakMode.byWordWrapping
        InformationView.addSubview(titleView)
        
        //profile image
        profileImage.frame = CGRect(x: InformationView.bounds.width-70, y: startYCG-15, width: 50, height: 50)
        let image = UIImage(named: imageProfile)
        profileImage.image = image
        
        //Subtitle
        keyTitle = UILabel(frame: CGRect(x: 10, y: startYCG+10, width: InformationView.bounds.width-20, height: 40))
        keyTitle.font = titleFont
        keyTitle.text = "Information of Duke Person"
        keyTitle.textColor = subTitleC
        InformationView.addSubview(keyTitle)
        
        //view of Key
        keyView = UIView(frame: CGRect(x: 10, y: startYCG+40, width: InformationView.bounds.width-20, height: 91))
        keyView.backgroundColor = keyFieldC
        keyView.layer.cornerRadius = 5
        keyTitle.layer.borderColor = UIColor.darkGray.cgColor
        InformationView.addSubview(keyView)
        
        //fistName
        firstText = UILabel(frame: CGRect(x: startPos1, y: startY+43, width: textWidth, height: textHeight))
        firstText.backgroundColor = keyFieldC
        firstText.font = textFont
        firstText.text = "First Name:* "
        InformationView.addSubview(firstText)
        
        firstField.frame = CGRect(x: startPos2, y: startYCG+43, width: InformationView.bounds.width-140, height: fieldHeight)
        firstField.backgroundColor = UIColor.white
        firstField.layer.cornerRadius = 2
        firstField.layer.borderColor = buttonBC
        firstField.placeholder = "First Name"
        InformationView.addSubview(firstField)
        
        //LastName
        lastText = UILabel(frame: CGRect(x: startPos1, y: startY+textHeight+48, width: textWidth, height: textHeight))
        lastText.backgroundColor = keyFieldC
        lastText.font = textFont
        lastText.text = "Last Name:* "
        InformationView.addSubview(lastText)
        
        lastField.frame = CGRect(x: startPos2, y: startYCG+fieldHeight+48, width: InformationView.bounds.width-140, height: fieldHeight)
        lastField.backgroundColor = UIColor.white
        lastField.layer.cornerRadius = 2
        lastField.layer.borderColor = buttonBC
        lastField.placeholder = "Last Name"
        InformationView.addSubview(lastField)
        
        //Team
        teamText = UILabel(frame: CGRect(x: startPos1, y: startY+textHeight*2+53, width: textWidth, height: textHeight))
        teamText.backgroundColor = keyFieldC
        teamText.font = textFont
        teamText.text = "Team Name:* "
        InformationView.addSubview(teamText)
        
        teamField = UITextField(frame: CGRect(x: startPos2, y: startYCG+fieldHeight*2+53, width: InformationView.bounds.width-140, height: fieldHeight))
        teamField.backgroundColor = UIColor.white
        teamField.layer.cornerRadius = 4
        teamField.layer.borderWidth = 0.5
        teamField.layer.borderColor = UIColor.lightGray.cgColor
        teamField.attributedPlaceholder = NSAttributedString(string: "Team Name", attributes: [NSFontAttributeName : fieldFont!])
        InformationView.addSubview(teamField)
        
        //gender
        genderText = UILabel(frame: CGRect(x: startPos1, y: startY+135, width: textWidth, height: textHeight))
        genderText.font = textFont
        genderText.text = "Gender:* "
        InformationView.addSubview(genderText)
        
        genderField.frame = CGRect(x: startPos2, y: startYCG+135, width: InformationView.bounds.width-140, height: fieldHeight)
        genderField.layer.borderWidth = 1
        genderField.layer.borderColor = buttonBC
        genderField.backgroundColor = UIColor.white
        genderField.layer.cornerRadius = 2
        genderField.attributedPlaceholder = NSAttributedString(string: "Male or Female", attributes: [NSFontAttributeName : fieldFont!])
        InformationView.addSubview(genderField)
        
        //role
        roleText = UILabel(frame: CGRect(x: startPos1, y: startY+textHeight+136, width: textWidth, height: textHeight))
        roleText.font = textFont
        roleText.text = "Role:* "
        InformationView.addSubview(roleText)
        
        roleField.frame = CGRect(x: startPos2, y: startYCG+fieldHeight+136, width: InformationView.bounds.width-140, height: fieldHeight)
        roleField.layer.borderWidth = 1
        roleField.layer.borderColor = buttonBC
        roleField.backgroundColor = UIColor.white
        roleField.layer.cornerRadius = 2
        roleField.attributedPlaceholder = NSAttributedString(string: "Student, Professor or TA", attributes: [NSFontAttributeName : fieldFont!])
        InformationView.addSubview(roleField)
        
        //whrer to from
        fromText = UILabel(frame: CGRect(x: startPos1, y: startY+2*(textHeight+1)+135, width: textWidth, height: textHeight))
        fromText.font = textFont
        fromText.text = "From:* "
        InformationView.addSubview(fromText)
        
        fromField.frame = CGRect(x: startPos2, y: startYCG+2*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        fromField.layer.borderWidth = 1
        fromField.layer.borderColor = buttonBC
        fromField.backgroundColor = UIColor.white
        fromField.layer.cornerRadius = 2
        fromField.attributedPlaceholder = NSAttributedString(string: "Any string of location info", attributes: [NSFontAttributeName : fieldFont!])
        InformationView.addSubview(fromField)
        
        //degree
        degreeText = UILabel(frame: CGRect(x: startPos1, y: startY+3*(textHeight+1)+135, width: textWidth, height: textHeight))
        degreeText.font = textFont
        degreeText.text = "Degree:* "
        InformationView.addSubview(degreeText)
        
        degreeField.frame = CGRect(x: startPos2, y: startYCG+3*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        degreeField.layer.borderWidth = 1
        degreeField.layer.borderColor = buttonBC
        degreeField.attributedPlaceholder = NSAttributedString(string: "Any string of degree info", attributes: [NSFontAttributeName : fieldFont!])
        degreeField.backgroundColor = UIColor.white
        degreeField.layer.cornerRadius = 2
        InformationView.addSubview(degreeField)
        
        // up to 3 hobbies
        hobby1Text = UILabel(frame: CGRect(x: startPos1, y: startY+4*(textHeight+1)+135, width: textWidth, height: textHeight))
        hobby1Text.font = textFont
        hobby1Text.text = "Hobby 1: "
        InformationView.addSubview(hobby1Text)
        
        hobby1Field.frame = CGRect(x: startPos2, y: startYCG+4*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        hobby1Field.layer.borderWidth = 1
        hobby1Field.layer.borderColor = buttonBC
        hobby1Field.attributedPlaceholder = NSAttributedString(string: "Any string of a hobby", attributes: [NSFontAttributeName : fieldFont!])
        hobby1Field.backgroundColor = UIColor.white
        hobby1Field.layer.cornerRadius = 2
        InformationView.addSubview(hobby1Field)
        
        hobby2Text = UILabel(frame: CGRect(x: startPos1, y: startY+5*(textHeight+1)+135, width: textWidth, height: textHeight))
        hobby2Text.font = textFont
        hobby2Text.text = "Hobby 2: "
        InformationView.addSubview(hobby2Text)
        
        hobby2Field.frame = CGRect(x: startPos2, y: startYCG+5*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        hobby2Field.layer.borderWidth = 1
        hobby2Field.layer.borderColor = buttonBC
        hobby2Field.attributedPlaceholder = NSAttributedString(string: "Any string of a hobby", attributes: [NSFontAttributeName : fieldFont!])
        hobby2Field.backgroundColor = UIColor.white
        hobby2Field.layer.cornerRadius = 2
        InformationView.addSubview(hobby2Field)
        
        hobby3Text = UILabel(frame: CGRect(x: startPos1, y: startY+6*(textHeight+1)+135, width: textWidth, height: textHeight))
        hobby3Text.font = textFont
        hobby3Text.text = "Hobby 3: "
        InformationView.addSubview(hobby3Text)
        
        hobby3Field.frame = CGRect(x: startPos2, y: startYCG+6*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        hobby3Field.layer.borderWidth = 1
        hobby3Field.layer.borderColor = buttonBC
        hobby3Field.attributedPlaceholder = NSAttributedString(string: "Any string of a hobby", attributes: [NSFontAttributeName : fieldFont!])
        hobby3Field.backgroundColor = UIColor.white
        hobby3Field.layer.cornerRadius = 2
        InformationView.addSubview(hobby3Field)
        
        //up to 3 preferred languages
        lang1Text = UILabel(frame: CGRect(x: startPos1, y: startY+7*(textHeight+1)+135, width: textWidth, height: textHeight))
        lang1Text.font = textFont
        lang1Text.text = "Language 1: "
        InformationView.addSubview(lang1Text)
        
        lang1Field.frame = CGRect(x: startPos2, y: startYCG+7*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        lang1Field.layer.borderWidth = 1
        lang1Field.layer.borderColor = buttonBC
        lang1Field.attributedPlaceholder = NSAttributedString(string: "Any string of a preferred language", attributes: [NSFontAttributeName : fieldFont!])
        lang1Field.backgroundColor = UIColor.white
        lang1Field.layer.cornerRadius = 2
        InformationView.addSubview(lang1Field)
        
        lang2Text = UILabel(frame: CGRect(x: startPos1, y: startY+8*(textHeight+1)+135, width: textWidth, height: textHeight))
        lang2Text.font = textFont
        lang2Text.text = "Language 2: "
        InformationView.addSubview(lang2Text)
        
        lang2Field.frame = CGRect(x: startPos2, y: startYCG+8*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        lang2Field.layer.borderWidth = 1
        lang2Field.layer.borderColor = buttonBC
        lang2Field.attributedPlaceholder = NSAttributedString(string: "Any string of a preferred language", attributes: [NSFontAttributeName : fieldFont!])
        lang2Field.backgroundColor = UIColor.white
        lang2Field.layer.cornerRadius = 2
        InformationView.addSubview(lang2Field)
        
        lang3Text = UILabel(frame: CGRect(x: startPos1, y: startY+9*(textHeight+1)+135, width: textWidth, height: textHeight))
        lang3Text.font = textFont
        lang3Text.text = "Language 3: "
        InformationView.addSubview(lang3Text)
        
        lang3Field.frame = CGRect(x: startPos2, y: startYCG+9*(fieldHeight+1)+135, width: InformationView.bounds.width-140, height: fieldHeight)
        lang3Field.layer.borderWidth = 1
        lang3Field.layer.borderColor = buttonBC
        lang3Field.attributedPlaceholder = NSAttributedString(string: "Any string of a preferred language", attributes: [NSFontAttributeName : fieldFont!])
        lang3Field.backgroundColor = UIColor.white
        lang3Field.layer.cornerRadius = 2
        InformationView.addSubview(lang3Field)
        
        //Upload Profile button
        //addButton = UIButton()
        addButton.backgroundColor = buttonC
        addButton.layer.cornerRadius = 5
        addButton.layer.borderWidth = 1
        addButton.layer.borderColor = buttonBC
        addButton.isHidden = false
        addButton.setTitle("Upload Image", for: .normal)
        addButton.setTitleColor(UIColor.black, for: .normal)
        addButton.setTitleColor(UIColor.gray, for: .selected)
        addButton.setTitleColor(subTitleC, for: .highlighted)
        addButton.frame = CGRect(x: startPos1, y: startY+400, width: 135, height: textHeight)
        //addButton.addTarget(self, action: #selector(NewPersonViewController.pressUploadProfile(_:)), for: .touchUpInside)
        addButton.showsTouchWhenHighlighted = true
        InformationView.addSubview(addButton)
        
        //clear button
        clearButton = UIButton()
        clearButton.backgroundColor = buttonC
        clearButton.layer.cornerRadius = 5
        clearButton.layer.borderWidth = 1
        clearButton.layer.borderColor = buttonBC
        clearButton.isHidden = false
        clearButton.setTitle("Clear", for: .normal)
        clearButton.setTitleColor(UIColor.black, for: .normal)
        clearButton.setTitleColor(UIColor.gray, for: .selected)
        clearButton.setTitleColor(subTitleC, for: .highlighted)
        clearButton.frame = CGRect(x: startPos1+146, y: startY+400, width: 140, height: textHeight)
        clearButton.addTarget(self, action: #selector(NewPersonViewController.pressClear(_:)), for: .touchUpInside)
        clearButton.showsTouchWhenHighlighted = true
        InformationView.addSubview(clearButton)

        //instruction
        insLabel = UILabel(frame: CGRect(x: 11, y: startYCG+425, width: InformationView.bounds.width-20, height: 20))
        insLabel.text = "Field marked with * is required for adding/updating persons."
        insLabel.font = UIFont(name: "Hiragino Sans", size: 10)
        insLabel.textColor = warnC
        InformationView.addSubview(insLabel)
        
        //result view
        resultLabel = UILabel(frame: CGRect(x: 10, y: startYCG+250, width: InformationView.bounds.width-20, height: 75))
        resultLabel.backgroundColor = UIColor.white
        resultLabel.layer.masksToBounds = true
        resultLabel.layer.cornerRadius = 2
        resultLabel.layer.borderWidth = 2
        resultLabel.layer.borderColor = buttonBC
        resultLabel.font = textFont
        resultLabel.allowsDefaultTighteningForTruncation = true
        resultLabel.adjustsFontSizeToFitWidth = true
        resultLabel.adjustsFontForContentSizeCategory = true
        resultLabel.numberOfLines = 0
        resultLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        resultLabel.textAlignment = NSTextAlignment.center
        resultLabel.minimumScaleFactor = CGFloat(0.1)
        resultLabel.text = ""
        resultLabel.isHidden = true
        InformationView.addSubview(resultLabel)
        
        //yes button
        yesButton = UIButton()
        yesButton.backgroundColor = UIColor.lightGray
        yesButton.layer.cornerRadius = 5
        yesButton.layer.borderWidth = 1
        yesButton.layer.borderColor = buttonBC
        yesButton.isHidden = true
        yesButton.setTitle("Yes", for: .normal)
        yesButton.setTitleColor(UIColor.black, for: .normal)
        yesButton.setTitleColor(UIColor.gray, for: .selected)
        yesButton.setTitleColor(subTitleC, for: .highlighted)
        yesButton.frame = CGRect(x: 138, y: startY+298, width: 40, height: textHeight-5)
        yesButton.addTarget(self, action: #selector(self.pressYes(_:)), for: .touchUpInside)
        yesButton.showsTouchWhenHighlighted = true
        InformationView.addSubview(yesButton)
        
        //save button
        saveButton = UIBarButtonItem(title:"Save",style: .plain, target: self, action: #selector(self.pressSave(_:)))
        self.navigationItem.rightBarButtonItem = saveButton
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //warning view
    func popWarning( warning : String) {
        resultLabel.isHidden = false
        resultLabel.text = warning
        yesButton.isHidden = false
    }
    
    //press Yes
    func pressYes(_ sender: UIButton!){
        resultLabel.isHidden = true
        yesButton.isHidden = true
    }

    func pressSave(_ sender: UIBarButtonItem!) {
        if ((firstField.text == "") || (lastField.text == "")) {
            self.popWarning(warning: "Invalid input: missing name")
            return
        }
        if ((genderField.text == "") || (roleField.text == "") || (degreeField.text == "") || (fromField.text == "")) {
            self.popWarning(warning: "Invalid input: missing infromation")
            return
        }
        if ((genderField.text != "Female") && (genderField.text != "Male")) {
            self.popWarning(warning: "Gender must be Female or Male")
            return
        }
        if ((roleField.text != "TA") && (roleField.text != "Student") && (roleField.text != "Professor")) {
            self.popWarning(warning: "Role must be TA, Student or Professor")
            return
        }
        performSegue(withIdentifier: "returnFromNewItem", sender: saveButton)

    }
    
    //reset text field view
    func originalColor(){
        firstText.textColor = UIColor.black
        lastText.textColor = UIColor.black
        genderText.textColor = UIColor.black
        roleText.textColor = UIColor.black
        fromText.textColor = UIColor.black
        degreeText.textColor = UIColor.black
    }
    
    //press upload profile
    func pressUploadProfile(_ sender: UIButton!) {}
    
    //press clear button
    func pressClear(_ sender: UIButton!) {
        originalColor()
        firstField.text?=""
        lastField.text?=""
        genderField.text?=""
        roleField.text?=""
        fromField.text?=""
        degreeField.text?=""
        hobby1Field.text?=""
        hobby2Field.text?=""
        hobby3Field.text?=""
        lang1Field?.text?=""
        lang2Field.text?=""
        lang3Field.text?=""
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ((sender as? UIBarButtonItem) != self.saveButton){
            return
        }
        //not null
        if (firstField.text=="")||(lastField.text=="")||(genderField.text=="")||(roleField.text=="")||(fromField.text=="")||(degreeField.text=="") {
            return
        }
        //invalid input
        if ((genderField.text != "Male") && (genderField.text != "Female"))||((roleField.text != "Professor") && (roleField.text != "TA") && (roleField.text != "Teaching Assistant") && (roleField.text != "Student")) {
            return
        }
        //built DukePerson
        var newGender = Gender.Male
        var newRole = DukeRole.Student
        if genderField.text=="Female" {
            newGender = Gender.Female
        }
        if (roleField.text == "TA") || (roleField.text == "Teaching Assistant") {
            newRole = DukeRole.TA
        }
        if roleField.text == "Professor" {
            newRole = DukeRole.Professor
        }
        var newHobby = [String]()
        if ((hobby1Field.text) != ""){
            newHobby.append(hobby1Field.text!)}
        if ((hobby2Field.text) != ""){
            newHobby.append(hobby2Field.text!)}
        if ((hobby3Field.text) != ""){
            newHobby.append(hobby3Field.text!)}
        var newLang = [String]()
        if ((lang1Field.text) != ""){
            newLang.append(lang1Field.text!)}
        if ((lang2Field.text) != ""){
            newLang.append(lang2Field.text!)}
        if ((lang3Field.text) != ""){
            newLang.append(lang3Field.text!)}
        self.newPerson = DukePerson(firstName: firstField.text!,
                                   lastName : lastField.text!,
                                   whereFrom : fromField.text!,
                                   gender : newGender,
                                   role : newRole,
                                   degree : degreeField.text!,
                                   hobbies : newHobby,
                                   programmingLanguage : newLang,
                                   teamName: teamField.text!)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
