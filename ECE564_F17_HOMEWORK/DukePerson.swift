//
//  DukePerson.swift
//  ECE564_F17_HOMEWORK
//
//  Created by 刘孟琳 on 10/3/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation

public enum Gender {
    case Male
    case Female
}
/*
class Person {
    var firstName = "First"
    var lastName = "Last"
    var whereFrom = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
}*/

public enum DukeRole : String {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}

protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}

class DukePerson : NSObject, BlueDevil, NSCoding {
    
    struct Keys {
        static let firstNameK = "firstNameK"
        static let lastNameK = "lastNameK"
        static let teamNameK = "teamNameK"
        static let whereFromK = "whereFromK"
        static let genderK = "genderK"
        static let roleK = "roleK"
        static let degreeK = "degreeK"
        static let hobbiesK = "hobbiesK"
        static let langsK = "langsK"
        
    }
    let space_joiner = " "
    var firstName = "First"
    var lastName = "Last"
    var whereFrom = "Anywhere"
    var gender : Gender = .Male

    var hobbies = [String]()
    var role : DukeRole = .Student
    var degree = "None"
    var programmingLanguage = [String]()
    var teamName = ""
    
    override init() {}
    
    //my own initializer
    init(firstName: String, lastName : String, whereFrom : String, gender : Gender, role : DukeRole, degree : String, hobbies : [String], programmingLanguage : [String], teamName : String) {
        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
        self.role = role
        self.degree = degree
        self.hobbies = hobbies
        self.programmingLanguage = programmingLanguage
        self.teamName = teamName
    }
    
    //2 - this decodes our objects; this isn't called explicitly, it will be called with NSKeyedArchiver
    required init(coder decoder: NSCoder) {
        //this retrieves our saved name object and casts it as a string
        if let firstNameObject = decoder.decodeObject(forKey: Keys.firstNameK) as? String {
            firstName = firstNameObject
        }
        if let lastNameObject = decoder.decodeObject(forKey: Keys.lastNameK) as? String {
            lastName = lastNameObject
        }
        if let teamNameObject = decoder.decodeObject(forKey: Keys.teamNameK) as? String {
            teamName = teamNameObject
        }
        if let fromObject = decoder.decodeObject(forKey: Keys.whereFromK) as? String {
            whereFrom = fromObject
        }
        if let genderObject = decoder.decodeObject(forKey: Keys.genderK) as? String {
            switch genderObject{
            case "Male":
                gender = Gender.Male
            default:
                gender = Gender.Female
            }
        }
        if let roleObject = decoder.decodeObject(forKey: Keys.roleK) as? String {
            switch roleObject{
            case "TA":
                role = DukeRole.TA
            case "Professor":
                role = DukeRole.Professor
            default:
                role = DukeRole.Student
            }
        }
        if let degreeObject = decoder.decodeObject(forKey: Keys.degreeK) as? String {
            degree = degreeObject
        }
        if let hobbyObject = decoder.decodeObject(forKey: Keys.hobbiesK) as? [String] {
            hobbies = hobbyObject
        }
        if let langObject = decoder.decodeObject(forKey: Keys.langsK) as? [String] {
            programmingLanguage = langObject
        }
    }
    
    //3 - this encodes our objects (saves them)
    func encode(with coder: NSCoder) {
        //we are saving the name for the key "name"
        coder.encode(firstName, forKey: Keys.firstNameK)
        coder.encode(lastName, forKey: Keys.lastNameK)
        coder.encode(teamName, forKey: Keys.teamNameK)
        coder.encode(degree, forKey: Keys.degreeK)
        coder.encode(whereFrom, forKey: Keys.whereFromK)
        coder.encode(hobbies, forKey: Keys.hobbiesK)
        coder.encode(programmingLanguage, forKey: Keys.langsK)
        var roleString = ""
        var genderString = ""
        switch role {
        case DukeRole.TA:
            roleString = "TA"
        case DukeRole.Professor:
            roleString = "Professor"
        default:
            roleString = "Student"
        }
        switch gender {
        case Gender.Female:
            genderString = "Female"
        default:
            genderString = "Male"
        }
        coder.encode(roleString, forKey: Keys.roleK)
        coder.encode(genderString, forKey: Keys.genderK)
    }
    
    override var description: String {
        var info = [String]()
        var heOrShe = "He"
        var hobby = ""
        var pLanguage = ""
        if (gender == .Female){
            heOrShe = "She"
        }
        info = [firstName, lastName, "is from", whereFrom, "and is a", role.rawValue + "."]
        
        //connect programming languages
        switch programmingLanguage.count {
        case 1:
            pLanguage = programmingLanguage[0]
        case 2:
            pLanguage = programmingLanguage[0] + " and " + programmingLanguage[1]
        case 3:
            pLanguage = programmingLanguage[0] + ", " + programmingLanguage[1] + " and " + programmingLanguage[2]
        default:
            pLanguage = ""
        }
        
        if pLanguage != "" {
            info += [heOrShe, "is proficient in", pLanguage + "."]
        }
        
        //connect hobbies
        switch hobbies.count {
        case 1:
            hobby = hobbies[0]
        case 2:
            hobby = hobbies[0] + " and " + hobbies[1]
        case 3:
            hobby = hobbies[0] + ", " + hobbies[1] + " and " + hobbies[2]
        default:
            hobby = ""
        }
        if hobby != "" {
            info += ["When not in class,", firstName, "enjoys", hobby + "."]
        }
        return info.joined(separator: space_joiner)

    }
}
