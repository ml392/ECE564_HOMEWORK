HW1
    - whoIs function implement to search DukePerson using full name
    - personList is a dictionary  which stores all DukePerson with their full name as key. Thus searching for person would be running in O(1) time
    - Input is just mock the first input data
    - Persons reads the first time input data
    - the return value would work when:
        - there is 0-3 hobbies
        - there is 0-3 languages
    - there are 4 tests
        - first one tests when whoIs work normally
        - second one tests the situation when "The person does not exist."
        - third one tests when there is no input for languages
        - last one tests when the number of hobbies is greater than 3, a error message will show up.

HW2
    -ViewController
        -Add/Update
            - the person would be added into the database if it is not existed before
            - the person would be updated in the database if it is existed before
            - an error message will display if required fields are not filled (marked with *), and the name of responsible fields will change to red
            - an error message will display if input is invalid for field Gender and Role, and the name of responsible fields will change to red
        -Clear
            - clear text fields
            - reset the color of all fields to black, which is implemented by the function originalColor
            - hide the result view
        - Search
            - search person by first name and last name
            - an error message will display if the first name or last name is not filled, and the name of responsible fields will change to red
    - DataModel
        - Modify whoIs to return the description of DukePerson
        - Besides whoIs, add tow more function to access the class
            - addPerson: allow user to add persons into dictionary personList
            - getPerson: get DukePerson if it exists

HW3
    -Update Page to update info
    -Add Page to add new person
    -Did not add any verification for data, if invalid, will just not add anything

HW4
    - The flip page disply the hobby of painting
    - Only Menglin Liu's page has has the "Flip" button'
    - Images including ferris wheel, background sky, canvas, brush
    - Animation
        - clouds can move steadly
        - brush can follow the contour of ferris wheel to move
        - ferris wheel can keep spinning
    - granphics context : painted ferris wheel in the canvas is drawed via custermized UIView class
    - Music: add a background music for animation view
HW5
    - implement the persistant data storage without image.
        - To load the initial data, please uncomment the line 65 "saveData()" at the file "DictionaryViewController.swift" and run once before you nomarlly run the app. Then comment the line 65 to run the app, and you will get the intial data "
    - implement the camera supports. But since I didn't save the image into data storage, and I reload the data each time I transfer page, the photoed image could only show in the "add" or "update" page
    - implement team seperator
    - implement the swipe to delete
    - implement the search page using the left bar button at the dictionary view
