//
//  UIViewController.swift
//  ECE564_F17_HOMEWORK
//
//  Created by 刘孟琳 on 9/18/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit

class NNewDukePersonViewController: UIViewController{
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var textField: UITextField!
    var dictionary = Dictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if ((sender as! UIBarButtonItem) != self.saveButton){
            return
        }
        if ((self.textField.text) != nil) {
            self.dictionary.firstName = self.textField.text!
            self.dictionary.completed = false
        }
        return
    }
    
    
}
